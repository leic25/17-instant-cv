const mongoose = require('mongoose');

const CurriculumSchema = new mongoose.Schema({
    userID: {
        type: String,
        required: true
    },
    path: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Curriculum', CurriculumSchema);