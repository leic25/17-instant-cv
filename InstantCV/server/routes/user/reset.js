const router       = require('express').Router();
const User         = require('../../model/User');
const bcrypt       = require('bcryptjs');
const nodemailer   = require('nodemailer');
const mailer       = require('../../functions/user/mailer');
const dotenv       = require('dotenv');
const controller   = require('../../controllers/userController');
const randomstring = require('randomstring');



//SEND EMAIL WITH SECRET TOKEN for verification
router.post('/', async (req, res) => {
  const user = await User.findOne({email: req.body.email});
  if(!user) {
      return res.status(400).send('User is not found');
  }

  const secretToken = randomstring.generate(7);
  try {
      const temp = await User.updateOne({email: req.body.email}, {secretToken: secretToken})
      const html =
          `Hello,
          <br/>
          Please verify your account by entering the following security code: <br/>
          <b> ${secretToken} </b> <br/>
          Have a nice day!`;

      const info = mailer( user.email, 'Please verify your email', html);
      res.json({'uspeh': user.email});
  } catch(err) {
      res.status(400).send(err);
  }
});

//CHECK IF SECRET TOKEN IS VALID
router.post('/token', async (req, res) => {
    console.log(req.body);
    const user = await User.findOne({email: req.body.email});

    if (!user)
        return res.status(400).send('User is not found');

    if (req.body.code != user.secretToken) {
        return res.status(401).send('Invalid security code');
    }
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);
    try {
        const upd = await User.updateOne({_id: user._id}, {password: hashedPassword, secretToken: ''});
        res.json({'uspehDrugiPut': 'nesto'});  // Da, Nikola :)
    } catch (err) {
        res.status(400).send(err);
    }

});

module.exports = router;
