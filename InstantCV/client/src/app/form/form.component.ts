import { Component, OnInit, Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { AuthService } from '../services/auth.service';

import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
@Injectable()
export class FormComponent implements OnInit {

  public logInForm: FormGroup;
  public signUpForm: FormGroup;

  validPass: boolean;
  validEmail: boolean;
  validName: boolean;
  validUser: boolean;

  public hide1 = true;
  public hide2 = true;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private http: HttpClient,
              private authService: AuthService){

      this.logInForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });

      this.signUpForm = this.formBuilder.group ({
      name:     ['', [Validators.required]],
      email:    ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });

      this.validUser = true;
      this.validPass = true;
      this.validEmail = true;
      this.validName = true;
  }

  ngOnInit(): void {
    const item = this.authService.getID();

    if (item) {
      this.router.navigateByUrl('/choose-template');
    }
  }

  /* Submit funcs  */
  public submitSign(data){
    console.log(data);

    if (!this.signUpForm.valid) {
      window.alert('Not valid!');
      return;
    }

    this.http.post<any>(environment.serverUrl + '/user/register', data).subscribe(data => {
     /* this.authService.setTokenWithExpiry('auth-token', data.userToken, 2000000);
      this.authService.setID(data.userId);*/
      console.log(data);
      this.router.navigateByUrl('/validate-account');
      this.logInForm.reset();
    },
    err => {
      console.log(err.error);
      this.validEmail = true;
      this.validName = true;

      if (err.error === 'Email already exists') {
        this.validEmail = false;
      }
      else if (err.error === 'Name already exists') {
        this.validName = false;
      }
    });

    // Sending data to server
  /*  this.http.post<any>(environment.serverUrl + '/user/register', data).subscribe(data => {

      this.authService.setTokenWithExpiry('auth-token', data.userToken, 2000000);
      this.authService.setID(data.userId);

      this.router.navigateByUrl('/choose-template');
      this.logInForm.reset();
    },
    err => {
      console.log(err.error);
      this.validEmail = true;
      this.validName = true;

      if (err.error === 'Email already exists') {
        this.validEmail = false;
      }
      else if (err.error === 'Name already exists') {
        this.validName = false;
      }
    });*/

  }

  public submitLog(data){
    console.log(data);

    if (!this.logInForm.valid) {
      window.alert('Not valid!');
      return;
    }

    // slanje serveru

    this.http.post<any>(environment.serverUrl + '/user/login', data).subscribe(data => {

      this.authService.setTokenWithExpiry('auth-token', data.userToken, 2000000);
      this.authService.setID(data.userId);

      this.router.navigateByUrl('/choose-template');
      this.logInForm.reset();
    },
    err => {
      console.log(err.error);
      this.validUser = true;
      this.validPass = true;

      if (err.error === 'Invalid password'){
        this.validPass = false;
      }
      else if (err.error === 'User don\'t exists'){
        this.validUser = false;
      }
    });
  }

  /* Getters */
  public logEmail(){
    return this.logInForm.get('email');
  }

  public logPass(){
    return this.logInForm.get('password');
  }

  public signName(){
    return this.signUpForm.get('name');
  }

  public signEmail(){
    return this.signUpForm.get('email');
  }

  public signPass(){
    return this.signUpForm.get('password');
  }

}
