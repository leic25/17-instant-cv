import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.css']
})
export class ValidationComponent implements OnInit {

  public validationForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private http: HttpClient,
              private authService: AuthService) {
    this.validationForm = this.formBuilder.group({
      code:     [''],
      email: ['']
        });
   }

  ngOnInit(): void {
  }

  public validate(data){
    console.log(data);

    this.http.post<any>(environment.serverUrl + '/user/register/validation', data).subscribe(data=>{
      this.authService.setTokenWithExpiry('auth-token', data.userToken, 2000000);
      this.authService.setID(data.userID);

      this.router.navigateByUrl('/choose-template');
    }, err => console.log(err));
  }

}
