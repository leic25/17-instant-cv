const fs = require('fs');
const transform = require('../functions/transform/transform');

async function JSONtoTEX(jsonData){
    console.log('from JSONtoTEX: ', jsonData);

    let addedLanguages = false;
    let cvColor = jsonData.Color === '' ? {r: 111, g: 156, b:45} : transform.hexToRgb(jsonData.Color);

    let intro =
    `\\documentclass[11pt,a4paper,sans]{moderncv}
    \\moderncvstyle{casual}
    \\moderncvcolor{${cvColor.r}, ${cvColor.g}, ${cvColor.b}}
    \\usepackage{lipsum}
    \\usepackage[scale=0.75]{geometry}

    \\firstname{${transform.escapeCharacters(jsonData.Name)}} % Your first name
    \\familyname{${transform.escapeCharacters(jsonData.LastName)}} % Your last name

    ${makeAddress(jsonData)}
    \\mobile{${transform.escapeCharacters(jsonData.Phone)}}
    \\email{${transform.escapeCharacters(jsonData.Email)}}
    \\photo[70pt][0.4pt]{${makePhoto(jsonData)}}
    %---------------------------------------

    \\begin{document}
    %---------------------------------------------------
    %	CURRICULUM VITAE
    %---------------------------------------------------

    \\makecvtitle

    `;

    let sectionsPart = '';
    const keys = Object.keys(jsonData);

    if(!addedLanguages && jsonData.Languages !== ''){
      sectionsPart += makeLanguages(transform.escapeCharacters(jsonData.Languages)) + '\n';
      addedLanguages = true;
    }


    for(let i = process.env.NUM_OF_STATIC_FIELDS; i < keys.length; i++){
        let section = keys[i];

        let sectionString = makeSection(section, jsonData[section]);
        sectionsPart += sectionString + '\n';
    }

    texString = intro + sectionsPart + '\\end{document}\n';

    await fs.writeFile(`./templates/template2/${jsonData.Name}${jsonData.LastName}.tex`,
     texString, function (err) {
        if(err) return console.log(err);
        console.log("Pravljenje tex fajla.");
    });
}


function makeSection(section, data){
    let retVal = '';
    let sectionStart = `\\section*{${section}}`;

    switch(section){
        case 'Education':
            retVal = makeEdu(data);
            break;
        case 'WorkExperience':
            sectionStart = `\\section*{Work experience}`
            retVal = makeExp(data);
            break;
        case 'Volunteering':
            retVal = makeVolunteering(data);
            break;
        case 'ProfessionalSkills':
            sectionStart = `\\section*{Professional skills}`
            retVal = makeSkills(data);
            break;
        case 'Projects':
            retVal = makeProjects(data);
           break;
        case 'Publications':
            retVal = makePublications(data);
            break;
        case 'Achievements':
            retVal = makeAchievements(data);
            break;
        case 'Talks':
            retVal = makeTalks(data);
            break;
        case 'Courses':
            retVal = makeCourses(data);
            break;
        case 'Interests':
            retVal = makeInterests(data);
            break;
        case 'Hobbies':
            retVal = makeHobbies(data);
            break;
        }

        return sectionStart + '\n' +retVal;
}

function makeAddress(data){
    let array = [];

    if(data.Address !== ''){
        array.push(` ${data.Address}`);
    }
    if(data.City !== ''){
        array.push(`${data.City}`);
    }
    if(data.Country !== ''){
        array.push(`${data.Country}`);
    }

    return array.length > 0 ? `\\address{${transform.escapeCharacters(data.Address)}}
    {${transform.escapeCharacters(data.City)}, ${transform.escapeCharacters(data.Country)}}` : '';
}

function makePhoto(data){
    return `${data.Picture}`;
}

function makeLanguages(languages){

    let section =
    `%LANGUAGES SECTION\n
     \\section{Languages}\n`;

    if(languages !== ''){
        langs = languages.split(', ');

        for(let lang in langs){
            language = langs[lang].substr(0, langs[lang].indexOf(' '));
            level = langs[lang].substr(langs[lang].indexOf(' ') + 1);

            section += `\\cvitemwithcomment{${language}}{${level}}{}\n`;
        }
    }

    return section;
}

function makeEdu(education){

    const keys = Object.keys(education);
    let eduList = '';


    for(let key in keys){
        eduList +=
        `\\cventry{${transform.escapeCharacters(education[keys[key]].year)}}
        {${transform.escapeCharacters(education[keys[key]].school)}}
        {}{}{}
        {${transform.escapeCharacters(education[keys[key]].description)}}

        `;
    }

    return eduList;
}

function makeExp(experiences){

    let expList = '';
    let keys = Object.keys(experiences);



    for(let key in keys){
        expList +=
        `\\cventry{${transform.escapeCharacters(experiences[keys[key]].year)}}
        {${transform.escapeCharacters(experiences[keys[key]].position)},}{}
        {${transform.escapeCharacters(experiences[keys[key]].company)}}{}
        {${transform.escapeCharacters(experiences[keys[key]].description)}}

        `;
    }

    return expList;
}

function makeVolunteering(volunteering){
    let volsList = '';
    const keys = Object.keys(volunteering);

    for(let key in keys){
        volsList +=
        `\\cventry{${transform.escapeCharacters(volunteering[keys[key]].year)}}{}{}
        {${transform.escapeCharacters(volunteering[keys[key]].company)}}{}
        {${transform.escapeCharacters(volunteering[keys[key]].description)}}

        `;
    }

    return volsList;
}

function makeProjects(projects){

    let projectsList = '';
    const keys = Object.keys(projects);

    for(let key in keys){
        projectsList +=
        `\\cventry{${transform.escapeCharacters(projects[keys[key]].year)}}
        {${transform.escapeCharacters(projects[keys[key]].name)},}{}
        {${transform.escapeCharacters(projects[keys[key]].type)}}{}
        {${transform.escapeCharacters(projects[keys[key]].description)}}

        `
    }

    return projectsList;
}

function makeSkills(skills){

    let skillList = '';
    let keys = Object.keys(skills);

    for(let key in keys){
      skillList +=
      `\\cventry{}{}{}{${transform.escapeCharacters(skills[keys[key]].skill)}}{}{}

      `;
    }

    return skillList;
}

function makeAchievements(achievements){

    let achievementsList = '';
    const keys = Object.keys(achievements);

    for(let key in keys){
        achievementsList +=
        `\\cventry{${transform.escapeCharacters(achievements[keys[key]].year)}}
        {${transform.escapeCharacters(achievements[keys[key]].name)}}{}{}{}
        {${transform.escapeCharacters(achievements[keys[key]].description)}}

        `;
    }

    return achievementsList;
}

function makeCourses(courses){

    let coursesList = '';
    const keys = Object.keys(courses);

    for(let key in keys){
        coursesList +=
        `\\cventry{${transform.escapeCharacters(courses[keys[key]].year)}}{}{}
        {${transform.escapeCharacters(courses[keys[key]].name)}}{}{}

        `;
    }

    return coursesList;
}

function makeHobbies(hobbies){
    const keys = Object.keys(hobbies);

    return `\\cventry{}{}{}{${transform.escapeCharacters(hobbies[keys[0]].description)}}{}{}`;
}

function makeInterests(interests){
    const keys = Object.keys(interests);

    return `\\cventry{}{}{}{${transform.escapeCharacters(interests[keys[0]].description)}}{}{}`;
}

function makePublications(publications){

    let publicationsList = '';
    const keys = Object.keys(publications);

    for(let key in keys){
        publicationsList +=
        `\\cventry{${transform.escapeCharacters(publications[keys[key]].year)}}
        {${transform.escapeCharacters(publications[keys[key]].name)}}{}{}{}
        {${transform.escapeCharacters(publications[keys[key]].description)}}

        `;
    }

    return publicationsList;
}


function makeTalks(talks){
    let talksList = '';
    const keys = Object.keys(talks);

    for(let key in keys){
        talksList +=
        `\\cventry{${transform.escapeCharacters(talks[keys[key]].year)}}{}{}
        {${transform.escapeCharacters(talks[keys[key]].description)}}{}{}

        `;
    }

    return talksList;
}


module.exports = {JSONtoTEX};
