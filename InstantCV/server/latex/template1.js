const fs = require('fs');
const transform = require('../functions/transform/transform');


async function JSONtoTEX(jsonData) {
    console.log('from JSONtoTEX: ', jsonData);

    let addedLanguages = false;

    let intro =
        `\\PassOptionsToPackage{dvipsnames}{xcolor}

        \\documentclass[10pt,a4paper,ragged2e]{altacv}

        \\geometry{left=1.25cm,right=1.25cm,top=1.5cm,bottom=1.5cm,columnsep=1.2cm}
        \\usepackage{paracol}
        % Change the font if you want to, depending on whether
        % you're using pdflatex or xelatex/lualatex
        \\ifxetexorluatex
            % If using xelatex or lualatex:
            \\setmainfont{Lato}
        \\else
            % If using pdflatex:
            \\usepackage[utf8]{inputenc}
            \\usepackage[T1]{fontenc}
            \\usepackage[default]{lato}
        \\fi
        \\maxdeadcycles=200

        \\definecolor{Mulberry}{HTML}{72243D}
        \\definecolor{SlateGrey}{HTML}{2E2E2E}
        \\definecolor{LightGrey}{HTML}{666666}
        \\colorlet{heading}{Sepia}
        \\colorlet{accent}{Mulberry}
        \\colorlet{emphasis}{SlateGrey}
        \\colorlet{body}{LightGrey}

        \\renewcommand{\\itemmarker}{{\\small\\textbullet}}
        \\renewcommand{\\ratingmarker}{\\faCircle}

        \\addbibresource{sample.bib}

        \\begin{document}
        \\name{${jsonData.Name} ${jsonData.LastName}}
        %% You can add multiple photos on the left or right
        \\photoR{2.8cm}{${makePhoto(jsonData)}}

        \\personalinfo{%
        ${makeAddress(jsonData)}
        ${makeEmail(jsonData)}
        ${makePhone(jsonData)}
        ${makeLinkedIn(jsonData)}
}

    \\makecvheader

    `;


    let sectionsPart = '';
    const keys = Object.keys(jsonData);


    if (!addedLanguages && jsonData.Languages !== '') {
        sectionsPart += makeLanguages(jsonData.Languages) + '\n';
        addedLanguages = true;
    }

    for (let i = process.env.NUM_OF_STATIC_FIELDS; i < keys.length; i++) {
        let section = keys[i];

        let sectionString = makeSection(section, jsonData[section]);
        sectionsPart += sectionString + '\n';
    }

    texString = intro + sectionsPart + '\\end{document}\n';


    await fs.writeFile(`./templates/template1/${jsonData.Name}${jsonData.LastName}.tex`,
        texString,
        function(err) {
            if (err) return console.log(err);
            console.log("Pravljenje tex fajla.");
        });
}

function makeAddress(data) {
    let array = [];

    if (data.Address !== '') {
        array.push(` ${data.Address}`);
    }

    if (data.City !== '') {
        array.push(`${data.City}`);
    }

    if (data.Country !== '') {
        array.push(`${data.Country}`);
    }

    return array.length > 0 ? `\\mailaddress{${transform.escapeCharacters(data.Address)}, ${transform.escapeCharacters(data.City)}, ${transform.escapeCharacters(data.Country)}}` : '';
}

function makePhone(data) {
    let array = [];

    if (data.Phone !== '') {
        array.push(` ${data.Phone}`);
    }

    return array.length > 0 ? `\\phone{${transform.escapeCharacters(data.Phone)}}` : '';
}

function makeLinkedIn(data) {
    let array = [];

    if (data.LinkedIn !== '') {
        array.push(` ${data.LinkedIn}`);
    }

    return array.length > 0 ? `\\linkedin{${transform.escapeCharacters(data.LinkedIn)}}` : '';
}


function makeEmail(data) {
    let array = [];

    if (data.Email !== '') {
        array.push(` ${data.Email}`);
    }

    return array.length > 0 ? `\\email{${transform.escapeCharacters(data.Email)}}` : '';
}

function makePhoto(data) {
    return `${data.Picture}`;
}

function makeLanguages(languages) {

    let section = '\\cvsection{Languages}\n';

    if (languages !== '') {

        langs = languages.split(', ');

        for (let lang in langs) {
            language = langs[lang].split(' ');
            section += `\\cvskill{${transform.escapeCharacters(language[0])}}{${transform.escapeCharacters(language[1])}}\n`;
            section += '\\divider';
        }
        section += '\\\\';
    }

    return section;
}

function makeSection(section, data) {
    let retVal = '';
    let sectionStart = `\\cvsection{${ section }}`;

    switch (section) {
        case 'Education':
            retVal = makeEdu(data);
            break;
        case 'WorkExperience':
            sectionStart = `\\cvsection{Work Experience}`;
            retVal = makeExp(data);
            break;
        case 'Volunteering':
            retVal = makeVolunteering(data);
            break;
        case 'ProfessionalSkills':
            sectionStart = `\\cvsection{Professional Skills}`
            retVal = makeSkills(data);
            break;
        case 'Projects':
            retVal = makeProjects(data);
            break;
        case 'Publications':
            retVal = makePublications(data);
            break;
        case 'Achievements':
            retVal = makeAchievements(data);
            break;
        case 'Talks':
            retVal = makeTalks(data);
            break;
        case 'Courses':
            retVal = makeCourses(data);
            break;
        case 'Interests':
            retVal = makeInterests(data);
            break;
        case 'Hobbies':
            retVal = makeHobbies(data);
            break;
    }

    return sectionStart + '\n' + retVal;
}

function makeEdu(education) {

    let eduList = '';
    const keys = Object.keys(education);

    for (let key in keys) {
        eduList += entry(education[keys[key]].year,
            education[keys[key]].school,
            education[keys[key]].description,
            '');
    }

    return eduList;
}

function makeExp(experiences) {

    let expList = '';
    let keys = Object.keys(experiences);

    for (let key in keys) {
        expList += entry(experiences[keys[key]].year,
            experiences[keys[key]].company,
            experiences[keys[key]].position,
            experiences[keys[key]].description);
    }

    return expList;
}

function makeVolunteering(volunteering) {
    let volsList = '';
    const keys = Object.keys(volunteering);

    for (let key in keys) {
        volsList += entry(volunteering[keys[key]].year,
            volunteering[keys[key]].company,
            volunteering[keys[key]].description,
            '');
    }

    return volsList;
}

function makeSkills(skills) {
    let skillArray = [];
    let keys = Object.keys(skills);

    for (let key in keys) {
        skillArray += `\\cvtag{${transform.escapeCharacters(skills[keys[key]].skill)}}\n`;
    }

    return skillArray + `\\\\ \\divider \n`;
}

function makeProjects(projects) {
    let projectsList = '';
    const keys = Object.keys(projects);

    for (let key in keys) {
        projectsList += entry(projects[keys[key]].year,
            projects[keys[key]].type,
            projects[keys[key]].name,
            projects[keys[key]].description);
    }

    return projectsList;
}

function makePublications(publications) {

    let publicationsList = '';
    const keys = Object.keys(publications);

    for (let key in keys) {
        publicationsList += entry(publications[keys[key]].year,
            publications[keys[key]].name,
            publications[keys[key]].description,
            '');
    }

    return publicationsList;
}

function makeAchievements(achievements) {

    let achievementsList = '';
    const keys = Object.keys(achievements);

    for (let key in keys) {
        achievementsList += entry(achievements[keys[key]].year,
            achievements[keys[key]].name,
            achievements[keys[key]].description, '');
    }

    return achievementsList;
}

function makeTalks(talks) {
    let talksList = '';
    const keys = Object.keys(talks);

    for (let key in keys) {
        talksList += entry(talks[keys[key]].year,
            talks[keys[key]].description, '', '');
    }

    return talksList;
}

function makeCourses(courses) {

    let coursesList = '';
    const keys = Object.keys(courses);

    for (let key in keys) {
        coursesList += entry(courses[keys[key]].year,
            courses[keys[key]].name,
            '',
            '');
    }

    return coursesList;
}

function makeInterests(interests) {
    const keys = Object.keys(interests);
    return transform.escapeCharacters(interests[keys[0]].description) + '\\\\ \\divider\n';
}

function makeHobbies(hobbies) {
    const keys = Object.keys(hobbies);

    return transform.escapeCharacters(hobbies[keys[0]].description) + '\\\\ \\divider\n';
}

function entry(param1, param2, param3, param4) {
    if (param4 !== '') {
        return `\\cvevent{${transform.escapeCharacters( param3 )}}{${transform.escapeCharacters( param2 )}}{${transform.escapeCharacters( param1 )}}{}
            \\begin{itemize}
            \\item ${transform.escapeCharacters(param4)}
            \\end{itemize}

            \\divider\\\\
            `;
    } else {

        return `\\cvevent{${transform.escapeCharacters(param2)}}{}{${transform.escapeCharacters(param1)}}{}
            ${transform.escapeCharacters(param3)}

            \\divider\\\\
        `;
    }
}

module.exports = { JSONtoTEX };
