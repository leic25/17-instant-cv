import { Injectable } from '@angular/core';
import { Template } from '../models/template.model';

@Injectable({
  providedIn: 'root'
})
export class TemplatesService {

  private templateWithPicture: Template[];
  private template: Template[];

  constructor() {

    this.templateWithPicture = [
      new Template(1, "assets/img/AltaCV.png"),
      new Template(2, "assets/img/Rizaly.png"),
      new Template(3, "assets/img/Roald.png"),
      new Template(4, "assets/img/Jan-CV.png"),
      new Template(5, "assets/img/Alice.png")
    ];

    this.template = [
      new Template(6, "assets/img/Lizen.png"),
      new Template(7, "assets/img/Patric.png"),
      new Template(8, "assets/img/Pastel.png"),
      new Template(9, "assets/img/Claud.png"),
      new Template(10, "assets/img/Jonh.png"),

      ];
  }

  public getTemplates(): Template[]{
    return this.template;
  }
  public getTemplatesWP(): Template[]{
    return this.templateWithPicture;
  }
}
