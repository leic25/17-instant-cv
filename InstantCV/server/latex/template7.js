const fs = require('fs');
const transform = require('../functions/transform/transform');

async function JSONtoTEX(jsonData) {
    console.log('from JSONtoTEX: ', jsonData);

    let addedLanguages = false;

    intro = `
    \\documentclass{tccv}
    \\usepackage[english]{babel}
    \\usepackage{fontawesome}

    \\begin{document}

    \\part{${jsonData.Name} ${jsonData.LastName}}

    {\\needspace{0.5\\textheight}%
    \\newdimen\\boxwidth%
    \\boxwidth=\\dimexpr\\linewidth-2\\fboxsep\\relax
    \\colorbox[HTML]{F5DD9D}{
    \\begin{tabularx}{\\boxwidth}{c|X}
    ${makeAddress(jsonData)}
    ${makeLinkedIn(jsonData)}
    ${makePhone(jsonData)}
    ${makeEmail(jsonData)}
    \\end{tabularx}
    }}

    `;

    let sectionsPart = '';
    const keys = Object.keys(jsonData);

    if (!addedLanguages && jsonData.Languages !== '') {
        sectionsPart += makeLanguages(jsonData.Languages) + '\n';
        addedLanguages = true;
    }

    for (let i = process.env.NUM_OF_STATIC_FIELDS; i < keys.length; i++) {
        let section = keys[i];
        let sectionString = makeSection(section, jsonData[section]);
        sectionsPart += sectionString + '\n';
    }


    texString = intro + sectionsPart + '\\end{document}\n';

    await fs.writeFile(`./templates/template7/${jsonData.Name}${jsonData.LastName}.tex`,
        texString,
        function(err) {
            if (err) return console.log(err);
            console.log("Pravljenje tex fajla.");
        });
}


function makeAddress(data) {
    let array = [];

    if (data.Address !== '') {
        array.push(` ${data.Address}`);
    }

    if (data.City !== '') {
        array.push(`${data.City}`);
    }

    if (data.Country !== '') {
        array.push(`${data.Country}`);
    }

    return array.length > 0 ? `\\raisebox{-2pt}{\\Large\\ding{45}} & {${transform.escapeCharacters(data.Address)}, ${transform.escapeCharacters(data.City)}, ${transform.escapeCharacters(data.Country)}}\\smallskip\\\\` : '';
}

function makePhone(data) {
    let array = [];

    if (data.Phone !== '') {
        array.push(` ${data.Phone}`);
    }

    return array.length > 0 ? `\\raisebox{-2pt}{\\Large\\ding{37}} & {${transform.escapeCharacters(data.Phone)}}\\smallskip\\\\` : '';
}

function makeLinkedIn(data) {
    let array = [];

    if (data.LinkedIn !== '') {
        array.push(` ${data.LinkedIn}`);
    }

    return array.length > 0 ? `\\raisebox{-2pt}{\\Large\\ding{218}} & {${transform.escapeCharacters(data.LinkedIn)}}\\smallskip\\\\` : '';
}


function makeEmail(data) {
    let array = [];

    if (data.Email !== '') {
        array.push(` ${data.Email}`);
    }

    return array.length > 0 ? `\\raisebox{-2pt}{\\Large\\ding{41}} & {${transform.escapeCharacters(data.Email)}}\\smallskip\\\\` : '';
}

function makeLanguages(languages) {

    let section = '\\section{Communication skills}\n';

    if (languages !== '') {

        langs = languages.split(', ');

        section += `\\begin{factlist}\n`

        for (let lang in langs) {
            language = langs[lang].split(' ');
            section += `\\item{${transform.escapeCharacters(language[0])}}{${transform.escapeCharacters(language[1])}}\n`;
        }
        section += '\\end{factlist}\n';
    }

    return section;
}

function makeSection(section, data) {
    let retVal = '';
    let sectionStart = `\\section{${section}}`;
    //  console.log("Section start: ", sectionStart);

    switch (section) {
        case 'Education':
            retVal = makeEdu(data);
            break;
        case 'WorkExperience':
            sectionStart = `\\section{Work experience}`
            retVal = makeExp(data);
            break;
        case 'Volunteering':
            retVal = makeVolunteering(data);
            break;
        case 'ProfessionalSkills':
            sectionStart = `\\section{Professional skills}`
            retVal = makeSkills(data);
            break;
        case 'Projects':
            retVal = makeProjects(data);
            break;
        case 'Publications':
            retVal = makePublications(data);
            break;
        case 'Achievements':
            retVal = makeAchievements(data);
            break;
        case 'Talks':
            retVal = makeTalks(data);
            break;
        case 'Courses':
            retVal = makeCourses(data);
            break;
        case 'Interests':
            retVal = makeInterests(data);
            break;
        case 'Hobbies':
            retVal = makeHobbies(data);
            break;

    }

    return sectionStart + '\n' + retVal;
}

function makeEdu(education) {

    let eduList = '';
    const keys = Object.keys(education);

    for (let key in keys) {
        eduList += entryYearList(education[keys[key]].year,
            education[keys[key]].school,
            education[keys[key]].description,
            '');
    }

    return yearList(eduList);
}

function makeExp(experience) {
    let expList = '';
    const keys = Object.keys(experience);

    for (let key in keys) {
        expList += entryEventList(experience[keys[key]].year,
            experience[keys[key]].company,
            experience[keys[key]].position,
            experience[keys[key]].description);
    }

    return eventList(expList);
}

function makeVolunteering(volunteering) {
    let volsList = '';
    const keys = Object.keys(volunteering);

    for (let key in keys) {
        volsList += entryYearList(volunteering[keys[key]].year,
            volunteering[keys[key]].company,
            volunteering[keys[key]].description, ' ');
    }

    return yearList(volsList);
}

function makeSkills(skills) {
    let skillsList = '\\begin{factlist}' + '\n' + `\\item{}{`;
    const keys = Object.keys(skills);

    for (let key in keys) {
        skillsList +=
            `${transform.escapeCharacters(skills[keys[key]].skill)} \\quad
        `;
    }

    skillsList += `}` + '\n' + `\\end{factlist}`;

    return skillsList + '~\n';
}

function makeProjects(projects) {
    let projectsList = '';
    const keys = Object.keys(projects);

    for (let key in keys) {
        projectsList += entryEventList(projects[keys[key]].year,
            projects[keys[key]].type,
            projects[keys[key]].name,
            projects[keys[key]].description);
    }

    return eventList(projectsList);
}

function makePublications(publications) {
    let publicationsList = '';
    const keys = Object.keys(publications);

    for (let key in keys) {
        publicationsList += entryYearList(publications[keys[key]].year,
            publications[keys[key]].name,
            publications[keys[key]].description, ' ');
    }

    return yearList(publicationsList);
}

function makeAchievements(achievements) {
    let achievementsList = '';
    const keys = Object.keys(achievements);

    for (let key in keys) {
        achievementsList += entryYearList(achievements[keys[key]].year,
            achievements[keys[key]].name,
            achievements[keys[key]].description, ' ');
    }

    return yearList(achievementsList);
}

function makeTalks(talks) {
    let talksList = '';
    const keys = Object.keys(talks);

    for (let key in keys) {
        talksList += entryYearList(talks[keys[key]].year,
            talks[keys[key]].description, ' ', ' ');
    }

    return yearList(talksList);
}

function makeCourses(courses) {
    let coursesList = '';
    const keys = Object.keys(courses);

    for (let key in keys) {
        coursesList += entryYearList(courses[keys[key]].year,
            courses[keys[key]].name, ' ', ' ');
    }

    return yearList(coursesList);
}

function makeInterests(interests) {
    const keys = Object.keys(interests);
    return transform.escapeCharacters(interests[keys[0]].description);
}

function makeHobbies(hobbies) {
    const keys = Object.keys(hobbies);

    return transform.escapeCharacters(hobbies[keys[0]].description);
}


function entryYearList(param1, param2, param3, param4) {

    return `\\item{${transform.escapeCharacters(param1)}}
            {${transform.escapeCharacters(param2)}}
            {${transform.escapeCharacters(param3)}}
            {${transform.escapeCharacters(param4)}}

        `
}

function entryEventList(param1, param2, param3, param4) {

    return `\\item{${transform.escapeCharacters(param1)}}
            {${transform.escapeCharacters(param2)}}
            {${transform.escapeCharacters(param3)}}

            ${transform.escapeCharacters(param4)}

        `
}

function yearList(data) {
    return `
    \\begin{yearlist}
        ${data}
    \\end{yearlist}
    `;
}

function eventList(data) {
    return `
    \\begin{eventlist}
        ${data}
    \\end{eventlist}
    `;
}

module.exports = { JSONtoTEX };
