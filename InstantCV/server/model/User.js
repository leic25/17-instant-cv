const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    picPath: {
        type: String,
        default: "profile.png"
    },
    secretToken: {
        type: String,
        required: false
    },
    validated: {
        type: Boolean,
        default: false,
        required: true
    }
});

module.exports = mongoose.model('User', userSchema);